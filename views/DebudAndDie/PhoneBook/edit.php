<?php
ini_set('display_errors', 'off');
error_reporting(E_ALL & ~E_DEPRECATED);
session_start();
include_once './../../../vendor/autoload.php';

use App\DebugAndDie\PhoneBook\Phone;
use App\DebugAndDie\Utility\Utility;

$phone = new Phone();

$phones = $phone->show($_GET['id']);
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Phone Book</title>

        <!-- Bootstrap -->
        <link href="./../../../asset/css/bootstrap.min.css" rel="stylesheet">
        <link href="./../../../asset/css/font-awesome.min.css" rel="stylesheet">
        <style>
            a {
                font-size: 16px;
                color: #f5f5f5;
            }
            a:hover{

                text-decoration: none;
                color: #fff;
            }
        </style>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!--font-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <section>
            <div class="container">
                <div class="row">

                    <div style="margin-top: 100px;border-radius: 10px;padding: 10px 20px 30px 20px;" class="col-md-offset-4 col-md-4 bg-warning">
                        <h3 class="text-center text-success"><span><i style="font-size: 24px;margin-right: 10px" class="fa fa-phone text-center text-success"></i>Update Name or Number</h3>
                        <hr style="border-top: 1px solid #e2e2e2;">
                        <form class="form-group text-center" action="update.php" method="post">
                            <div class="form-group">

                                <input type="hidden" class="form-control"  name="id" id="exampleInputName2" value="<?php echo $phones['id']; ?>" >
                                <label class="text-success" for="exampleInputName2">Enter Name: </label>
                                <input type="text" class="form-control"  name="name" id="exampleInputName2" value="<?php echo $phones['name']; ?>" >
                                <label class="text-success" for="exampleInputName2">Enter Number: </label>
                                <input type="number" class="form-control"  name="number" id="exampleInputName2" value="<?php echo $phones['number']; ?>" >
                            </div>

                            <button type="submit" class="btn btn-warning btn-lg btn-block">Update</button>
                            <button class="btn btn-primary btn-lg  btn-block"> <a href="list.php">Go to List</a></button>
                        </form>

                    </div>


                </div>
            </div>
        </section>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="./../../../asset/js/bootstrap.min.js"></script>

    </body>
</html>