
<?php
ini_set('display_errors', 'off');
error_reporting(E_ALL & ~E_DEPRECATED);
session_start();
include_once './../../../vendor/autoload.php';

use App\DebugAndDie\PhoneBook\Phone;
use App\DebugAndDie\Utility\Utility;

$phone = new Phone();
$phones = $phone->trashed();
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Phone Book</title>

        <!-- Bootstrap -->
        <link href="./../../../asset/css/bootstrap.min.css" rel="stylesheet">

        <style>
            a {
                font-size: 16px;
                color: #f5f5f5;
            }
            a:hover{

                text-decoration: none;
                color: #fff;
            }
            .no a {
                color: #000;
            }
            #utility{

                float:right;
                width:60%;

            }
            #message {
                background: #66afe9;
                color: #fff;
                margin: 15px;

            }

        </style>


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!--font-->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <section>
            <div class="container">
                <ins><h3 class="text-center">Trashed Phone Number List</h3></ins> 


                <div id="message" >
                    <h3> <?php echo Utility::message(); ?></h3> 

                </div>
                <div class="row">
                    <form action="recovermultiple.php" method="post">
                        <div>
                            <span>  <button type="submit">Recover ALL</button></span>

                            <span><button type="submit" id="deleteall">Delete ALL</button></span>
                        </div>
                        <br>



                        <table class="table table-bordered text-center bg-info">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" name="markall" id="markall" value=""></th>
                                    <th class="text-center">Sl.</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Phone Number &dArr;</th>
                                    <th class="text-center" colspan="5">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php
                                if (count($phones) > 0) {
                                    $slno = 1;
                                    foreach ($phones as $phone) {
                                        ?>
                                        <tr>
                                            <th><input type="checkbox" class="mark" name="mark[]" value="<?php echo $phone->id; ?>"></th>
                                            <td><?php echo $slno; ?></td>
                                            <td class="no"><a href="show.php?id=<?php echo $phone->id; ?>"><?php echo $phone->name; ?></a></td>
                                            <td class="no" style="text-transform: uppercase;"><a href="show.php?id=<?php echo $phone->id; ?>"><?php echo $phone->number; ?></a></td>

                                            <td><button class="btn btn-warning btn-md"><a href="recover.php?id=<?php echo $phone->id; ?>">Recover</a></button></td>
                                            <td><button class="btn btn-danger btn-md"><a href="delete.php?id=<?php echo $phone->id; ?>">Delete</a></button></td>


                                            <!--                            <form action="delete.php" method="post">
                                                                            <input type="hidden" name="id" value="<?php echo $phone->id; ?>"/>
                                                                            <td><button type="submit" class="btn btn-danger btn-md delete">Delete</button></td>
                                                                        </form>-->




                                        </tr>
                                        <?php
                                        $slno++;
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="6">
                                            <h2>No Result Found.</h2>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </form>
                </div>

                <div class="row text-center">      
                    <nav>

                        <ul class="pagination my-navigation">
                            <li>
                                <a href="#" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                            </li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li>
                                <a href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <nav style="padding: 0px 10px 0px 10px" class="text-center list-unstyled list-inline">
                        <li class="pull-left"><button class="btn btn-sm btn-primary"> <a href="javascript:history.go(-1)">Back</a></button></li>
                        <li class="pull-right"><button class="btn btn-sm btn-primary"> <a href="list.php">Go to List</a></button></li>

                    </nav>

                </div>

        </section>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="./../../../asset/js/bootstrap.min.js"></script>

        <script>
            $(document).ready(function () {
                $('.delete').bind('click', function (e) {
                    var deleteItem = confirm("Are You Sure You Want to Delete?");
                    if (!deleteItem) {
                        //return false;
                        e.preventDefault();
                    }
                });
                $('#message').fadeOut(6000);

                $('#markall').bind('click', function () {
                    if ($('#markall').is(':checked')) {
                        $('.mark').each(function () {
                            this.checked = true;

                        });


                    } else {

                        $('.mark').each(function () {
                            this.checked = false;

                        });

                    }

                });
                $('#deleteall').bind('click', function (e) {

                    var startDeleteProcess = false;
                    $('.mark').each(function () { //loop through each checkbox
                        if ($(this).is(':checked')) {
                            startDeleteProcess = true;
                        }
                    });

                    if (startDeleteProcess) {
                        var deleteItem = confirm("Are you sure you want to delete all Items??");
                        if (deleteItem) {
                            document.forms[0].action = 'deletemultiple.php';
                            document.forms[0].submit();
                        }
                    } else {
                        alert("Please select an item first");
                    }
                });
            });
        </script>

    </body>
</html>
