<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Registration Form </title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="./../../../asset/css/bootstrap.min.css">
        <link rel="stylesheet" href="./../../../asset/css/font-awesome.min.css">
        <link rel="stylesheet" href="./../../../asset/css/form-elements.css">
        <link rel="stylesheet" href="./../../../asset/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <!-- Top content -->
        <div class="top-content">

            <div class="inner-bg">
                <div class="container">
                    <div class="row ">
                        <div style="margin-top: 50px;" class="col-sm-7 text">
                            <h1><strong>Debug&Die PhoneBook</strong><br> Registration Form</h1>

                            <div class="top-big-link">
                                <a class="btn btn-link-1" href="./../../../index.php">Go to Home</a>
                                <a class="btn btn-link-2" href="./../../../views/DebudAndDie/PhoneBook/index.php">Login</a>
                            </div>
                        </div>
                        <div style="margin-top: -10px" class="col-sm-5 form-box">
                            <div class="form-top">
                                <div class="form-top-left">
                                    <h3>Sign up now</h3>
                                    <p>Fill in the form below to get registered.</p>
                                </div>

                            </div>
                            <div class="form-bottom">
                                <form role="form" action="reg_action.php" method="post" class="registration-form">
                                    <div class="form-group">
                                        <label class="sr-only" for="form-first-name">User name</label>
                                        <input type="text" name="username" placeholder="User name..." class="form-first-name form-control" id="form-first-name">
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="form-first-name">Password</label>
                                        <input type="password" name="password" placeholder="Enter Password..." class="form-first-name form-control" id="form-first-name">
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="form-first-name">First name</label>
                                        <input type="text" name="fname" placeholder="First name..." class="form-first-name form-control" id="form-first-name">
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="form-last-name">Sure name</label>
                                        <input type="text" name="sname" placeholder="Sure name..." class="form-sure-name form-control" id="form-last-name">
                                    </div>

                                    <button type="submit" class="btn">Sign me up!</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <!-- Javascript -->
        <script src="./../../../asset/js/jquery-1.11.1.min.js"></script>
        <script src="./../../../asset/js/bootstrap.min.js"></script>
        <script src="./../../../asset/js/jquery.backstretch.min.js"></script>
        <script src="./../../../asset/js/scripts.js"></script>

        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>
